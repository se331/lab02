import { TestBed, inject } from '@angular/core/testing';

import { StudentDataImpl2Service } from './student-data-impl-2.service';

describe('StudentDataImpl2Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StudentDataImpl2Service]
    });
  });

  it('should be created', inject([StudentDataImpl2Service], (service: StudentDataImpl2Service) => {
    expect(service).toBeTruthy();
  }));
});

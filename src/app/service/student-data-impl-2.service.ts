import { Injectable } from '@angular/core';
import { StudentService } from './student-service';
import { Student } from '../entity/student';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentDataImpl2Service extends StudentService{

  constructor() {
    super()
  }

  getStudents(): Observable<Student[]>{
    return of(this.students);
  };

  students: Student[] = [{
    'id': 3,
    'studentId': '602115012',
    'name': 'Thitiwut',
    'surname': 'Chutipongwanit',
    'gpa': 0.36
  }];
}
